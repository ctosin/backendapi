package de.cocus.flightinportugal.backendapi.controller;

import static org.hamcrest.CoreMatchers.containsStringIgnoringCase;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import de.cocus.flightinportugal.backendapi.application.service.LogService;
import de.cocus.flightinportugal.backendapi.domain.model.log.RequestLog;
import de.cocus.flightinportugal.backendapi.utils.Constants;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class FlightSearchControllerTest {	
	private static final String PATH_FLIGHTS_AVERAGE = "/flight/avg";
	private static final String PATH_LIST_LOGS = "/flight/logs";
	
	private static LocalDate dateFrom;
	private static LocalDate dateTo;
	private static LocalDate dateInPast;

	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private LogService logService;
	
	/**
	 * Data setup before running the tests
	 */
	@BeforeAll
	public static void setup() {
		dateFrom = LocalDate.now().plusDays(1);
		dateTo = dateFrom.plusDays(7);
		dateInPast = dateFrom.minusDays(5);
	}
	
	/**
	 * Tests validation error when no currency is provided
	 * @throws Exception
	 */
	@Test
	public void whenCallingWithNoCurrencyThenValidationError() throws Exception {
		mvc.perform(get(PATH_FLIGHTS_AVERAGE)
				.queryParam("airportFrom", "LIS")
				.queryParam("airportTo", "OPO")
				.queryParam("dateFrom", Constants.DATE_FORMATTER.format(dateFrom))
				.queryParam("dateTo", Constants.DATE_FORMATTER.format(dateTo))
			).andExpect(status().isBadRequest())
			.andExpect(jsonPath("$.code", is(400)))
			.andExpect(jsonPath("$.success", is(false)))
			.andExpect(jsonPath("$.errors", hasSize(1)))
			.andExpect(jsonPath("$.errors[0]", containsStringIgnoringCase("currency")));
	}
	
	/**
	 * Tests validation error when no airport from is provided
	 * @throws Exception
	 */
	@Test
	public void whenCallingWithNoAirportFromThenValidationError() throws Exception {
		mvc.perform(get(PATH_FLIGHTS_AVERAGE)
				.queryParam("airportTo", "OPO")
				.queryParam("dateFrom", Constants.DATE_FORMATTER.format(dateFrom))
				.queryParam("dateTo", Constants.DATE_FORMATTER.format(dateTo))
				.queryParam("currency", "EUR")
			).andExpect(status().isBadRequest())
			.andExpect(jsonPath("$.code", is(400)))
			.andExpect(jsonPath("$.success", is(false)))
			.andExpect(jsonPath("$.errors", hasSize(1)))
			.andExpect(jsonPath("$.errors[0]", containsStringIgnoringCase("from airport")));
	}
	
	/**
	 * Tests validation error when no airport to is provided
	 * @throws Exception
	 */
	@Test
	public void whenCallingWithNoAirportToThenValidationError() throws Exception {
		mvc.perform(get(PATH_FLIGHTS_AVERAGE)
				.queryParam("airportFrom", "LIS")
				.queryParam("dateFrom", Constants.DATE_FORMATTER.format(dateFrom))
				.queryParam("dateTo", Constants.DATE_FORMATTER.format(dateTo))
				.queryParam("currency", "EUR")
			).andExpect(status().isBadRequest())
			.andExpect(jsonPath("$.code", is(400)))
			.andExpect(jsonPath("$.success", is(false)))
			.andExpect(jsonPath("$.errors", hasSize(1)))
			.andExpect(jsonPath("$.errors[0]", containsStringIgnoringCase("to airport")));
	}
	
	/**
	 * Tests validation error when no date from is provided
	 * @throws Exception
	 */
	@Test
	public void whenCallingWithNoDateFromThenValidationError() throws Exception {
		mvc.perform(get(PATH_FLIGHTS_AVERAGE)
				.queryParam("airportFrom", "LIS")
				.queryParam("airportTo", "OPO")
				.queryParam("dateTo", Constants.DATE_FORMATTER.format(dateTo))
				.queryParam("currency", "EUR")
			).andExpect(status().isBadRequest())
			.andExpect(jsonPath("$.code", is(400)))
			.andExpect(jsonPath("$.success", is(false)))
			.andExpect(jsonPath("$.errors", hasSize(1)))
			.andExpect(jsonPath("$.errors[0]", containsStringIgnoringCase("from date")));
	}
	
	/**
	 * Tests validation error when no date to is provided
	 * @throws Exception
	 */
	@Test
	public void whenCallingWithNoDateToThenValidationError() throws Exception {
		mvc.perform(get(PATH_FLIGHTS_AVERAGE)
				.queryParam("airportFrom", "LIS")
				.queryParam("airportTo", "OPO")
				.queryParam("dateFrom", Constants.DATE_FORMATTER.format(dateFrom))
				.queryParam("currency", "EUR")
			).andExpect(status().isBadRequest())
			.andExpect(jsonPath("$.code", is(400)))
			.andExpect(jsonPath("$.success", is(false)))
			.andExpect(jsonPath("$.errors", hasSize(1)))
			.andExpect(jsonPath("$.errors[0]", containsStringIgnoringCase("to date")));
	}
	
	
	/**
	 * Tests validation error when an invalid date from is provided
	 * @throws Exception
	 */
	@Test
	public void whenCallingWithInvalidDateFromThenValidationError() throws Exception {
		mvc.perform(get(PATH_FLIGHTS_AVERAGE)
				.queryParam("airportFrom", "LIS")
				.queryParam("airportTo", "OPO")
				.queryParam("dateFrom", "abc")
				.queryParam("dateTo", Constants.DATE_FORMATTER.format(dateTo))
				.queryParam("currency", "EUR")
			).andExpect(status().isBadRequest())
			.andExpect(jsonPath("$.code", is(400)))
			.andExpect(jsonPath("$.success", is(false)))
			.andExpect(jsonPath("$.errors", hasSize(1)))
			.andExpect(jsonPath("$.errors[0]", containsStringIgnoringCase("failed to convert")));
	}
	
	/**
	 * Tests validation error when an invalid date to is provided
	 * @throws Exception
	 */
	@Test
	public void whenCallingWithInvalidDateToThenValidationError() throws Exception {
		mvc.perform(get(PATH_FLIGHTS_AVERAGE)
				.queryParam("airportFrom", "LIS")
				.queryParam("airportTo", "OPO")
				.queryParam("dateFrom", Constants.DATE_FORMATTER.format(dateFrom))
				.queryParam("dateTo", "abc")
				.queryParam("currency", "EUR")
			).andExpect(status().isBadRequest())
			.andExpect(jsonPath("$.code", is(400)))
			.andExpect(jsonPath("$.success", is(false)))
			.andExpect(jsonPath("$.errors", hasSize(1)))
			.andExpect(jsonPath("$.errors[0]", containsStringIgnoringCase("failed to convert")));
	}
	
	/**
	 * Tests validation error when date to in past is provided
	 * @throws Exception
	 */
	@Test
	public void whenCallingWithDateToInPastThenValidationError() throws Exception {
		mvc.perform(get(PATH_FLIGHTS_AVERAGE)
				.queryParam("airportFrom", "LIS")
				.queryParam("airportTo", "OPO")
				.queryParam("dateFrom", Constants.DATE_FORMATTER.format(dateFrom))
				.queryParam("dateTo", Constants.DATE_FORMATTER.format(dateInPast))
				.queryParam("currency", "EUR")
			).andExpect(status().isBadRequest())
			.andExpect(jsonPath("$.code", is(400)))
			.andExpect(jsonPath("$.success", is(false)))
			.andExpect(jsonPath("$.errors", hasSize(1)))
			.andExpect(jsonPath("$.errors[0]", containsStringIgnoringCase("future")));
	}
	
	/**
	 * Tests validation error when date from in past is provided
	 * @throws Exception
	 */
	@Test
	public void whenCallingWithDateFromInPastThenValidationError() throws Exception {
		mvc.perform(get(PATH_FLIGHTS_AVERAGE)
				.queryParam("airportFrom", "LIS")
				.queryParam("airportTo", "OPO")
				.queryParam("dateFrom", Constants.DATE_FORMATTER.format(dateInPast))
				.queryParam("dateTo", Constants.DATE_FORMATTER.format(dateTo))
				.queryParam("currency", "EUR")
			).andExpect(status().isBadRequest())
			.andExpect(jsonPath("$.code", is(400)))
			.andExpect(jsonPath("$.success", is(false)))
			.andExpect(jsonPath("$.errors", hasSize(1)))
			.andExpect(jsonPath("$.errors[0]", containsStringIgnoringCase("future")));
	}
	
	/**
	 * Tests validation error when an invalid airport from is provided
	 * @throws Exception
	 */
	@Test
	public void whenCallingWithAirportFromInvalidThenValidationError() throws Exception {
		mvc.perform(get(PATH_FLIGHTS_AVERAGE)
				.queryParam("airportFrom", "LIZ")
				.queryParam("airportTo", "OPO")
				.queryParam("dateFrom", Constants.DATE_FORMATTER.format(dateFrom))
				.queryParam("dateTo", Constants.DATE_FORMATTER.format(dateTo))
				.queryParam("currency", "EUR")
			).andExpect(status().isBadRequest())
			.andExpect(jsonPath("$.code", is(400)))
			.andExpect(jsonPath("$.success", is(false)))
			.andExpect(jsonPath("$.errors", hasSize(1)))
			.andExpect(jsonPath("$.errors[0]", containsStringIgnoringCase("not recognized location")));
	}
	
	/**
	 * Tests validation error when an invalid airport to is provided
	 * @throws Exception
	 */
	@Test
	public void whenCallingWithAirportToInvalidThenValidationError() throws Exception {
		mvc.perform(get(PATH_FLIGHTS_AVERAGE)
				.queryParam("airportFrom", "LIS")
				.queryParam("airportTo", "OPQ")
				.queryParam("dateFrom", Constants.DATE_FORMATTER.format(dateFrom))
				.queryParam("dateTo", Constants.DATE_FORMATTER.format(dateTo))
				.queryParam("currency", "EUR")
			).andExpect(status().isBadRequest())
			.andExpect(jsonPath("$.code", is(400)))
			.andExpect(jsonPath("$.success", is(false)))
			.andExpect(jsonPath("$.errors", hasSize(1)))
			.andExpect(jsonPath("$.errors[0]", containsStringIgnoringCase("not recognized location")));
	}
	
	/**
	 * Tests success
	 * @throws Exception
	 */
	@Test
	public void whenCallingWithCorrectParametersThenSuccess() throws Exception {
		mvc.perform(get(PATH_FLIGHTS_AVERAGE)
				.queryParam("airportFrom", "LIS")
				.queryParam("airportTo", "OPO")
				.queryParam("dateFrom", Constants.DATE_FORMATTER.format(dateFrom))
				.queryParam("dateTo", Constants.DATE_FORMATTER.format(dateTo))
				.queryParam("currency", "EUR")
			).andExpect(status().isOk())
			.andExpect(jsonPath("$.code", is(200)))
			.andExpect(jsonPath("$.success", is(true)))
			.andExpect(jsonPath("$.data.date_from", is(dateFrom.format(Constants.DATE_FORMATTER))))
			.andExpect(jsonPath("$.data.date_to", is(dateTo.format(Constants.DATE_FORMATTER))))
			.andExpect(jsonPath("$.data.airport_from", equalTo("LIS")))
			.andExpect(jsonPath("$.data.airport_to", equalTo("OPO")))
			.andExpect(jsonPath("$.data.flight_price_avg", hasSize(greaterThan(0))));
	}
	
	/**
	 * Tests the request log listing
	 * @throws Exception
	 */
	@Test
	public void whenListingLogs() throws Exception {
		// Creates 10 dummy logs
		List<RequestLog> logs = new ArrayList<>();
		for (int i = 1; i <= 10; i++) {
			RequestLog requestLog = new RequestLog();
			requestLog.setId(Long.valueOf(i));
			requestLog.setLogTime(LocalDateTime.now());
			requestLog.setIpAddress("200.200.200.200");
			requestLog.setCachedResponse(false);
			requestLog.setRequestData("some request data");
			requestLog.setResponseData("some response data");
			logs.add(requestLog);
		}
		
		// Return the dummy logs when LogService.listAllRequestLogs() is called
		Mockito.when(logService.listAllRequestLogs()).thenReturn(logs);
		
		mvc.perform(get(PATH_LIST_LOGS))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.code", is(200)))
			.andExpect(jsonPath("$.success", is(true)))
			.andExpect(jsonPath("$.data", hasSize(10)));
		
		mvc.perform(delete(PATH_LIST_LOGS))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.code", is(200)));
		
		Mockito.when(logService.listAllRequestLogs()).thenReturn(new ArrayList<>());
		
		mvc.perform(get(PATH_LIST_LOGS))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.code", is(200)))
			.andExpect(jsonPath("$.success", is(true)))
			.andExpect(jsonPath("$.data", hasSize(0)));
	}
}
