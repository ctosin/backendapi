package de.cocus.flightinportugal.backendapi.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import de.cocus.flightinportugal.backendapi.application.service.FlightServiceRequest;
import de.cocus.flightinportugal.backendapi.application.service.LogService;
import de.cocus.flightinportugal.backendapi.domain.model.flight.Flight;
import de.cocus.flightinportugal.backendapi.domain.model.log.RequestLog;
import de.cocus.flightinportugal.backendapi.domain.model.log.RequestLogRepository;

@SpringBootTest
@ActiveProfiles("test")
class LogServiceTest {
	private static final String IP_ADDRESS = "255.255.255.255";
	
	private static FlightServiceRequest request;
	
	private static Flight flight;
	
	@Autowired
	private LogService logService;
	
	@Autowired
	private RequestLogRepository logRepository;
	
	
	/**
	 * Creates some dummy info
	 */
	public static void setup() {
		String airportFrom = "LIS";
		String airportTo = "OPO";
		LocalDate dateFrom = LocalDate.now().plusDays(1);
		LocalDate dateTo = dateFrom.plusDays(7);
		String currency = "EUR";
		
		request = new FlightServiceRequest(airportFrom, airportTo, dateFrom, dateTo, currency, IP_ADDRESS);
		flight = new Flight();
		flight.setAirportFrom(airportFrom);
		flight.setAirportTo(airportTo);
		flight.setDateFrom(LocalDate.now());
		flight.setDateTo(LocalDate.now());
	}
	
	/**
	 * Tests the request log insertion service method, then uses the repository to check data
	 * @throws Exception
	 */
	@Test
	public void whenLogIsInserted() throws Exception {
		boolean cachedResponse = false;
		RequestLog log = logService.logRequest(IP_ADDRESS, request, flight, cachedResponse);
		
		assertThat(log).isNotNull();
		assertThat(log.getId()).isNotNull();
		
		Long logId = log.getId();
		
		RequestLog logFromDB = logRepository.findById(logId).orElseThrow();
		assertThat(logFromDB).isNotNull();
		
		assertThat(logFromDB.getIpAddress()).isEqualTo(log.getIpAddress());
		
		assertThat(logFromDB.getCachedResponse()).isEqualTo(cachedResponse);
	}

	/**
	 * Tests the insertion and deletion of request logs
	 * @throws Exception
	 */
	@Test
	public void whenLogsAreInsertedAndDeleted() throws Exception {
		assertThat(logService).isNotNull();
		logService.deleteAllRequestLogs();
		
		assertThat(logService.listAllRequestLogs()).isEmpty();
		
		logService.logRequest(IP_ADDRESS, request, flight, false);
		logService.logRequest(IP_ADDRESS, request, flight, false);
		logService.logRequest(IP_ADDRESS, request, flight, false);
		logService.logRequest(IP_ADDRESS, request, flight, false);
		logService.logRequest(IP_ADDRESS, request, flight, false);
		
		assertThat(logService.listAllRequestLogs()).hasSize(5);
		
		logService.deleteAllRequestLogs();
		
		assertThat(logService.listAllRequestLogs()).isEmpty();
	}
}
