package de.cocus.flightinportugal.backendapi.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import de.cocus.flightinportugal.backendapi.domain.model.flight.Flight;
import de.cocus.flightinportugal.backendapi.domain.model.flight.FlightData;
import de.cocus.flightinportugal.backendapi.domain.model.flight.Price;
import de.cocus.flightinportugal.backendapi.domain.service.flight.PricesCalculator;
import de.cocus.flightinportugal.backendapi.utils.NumberUtils;

@SpringBootTest
@ActiveProfiles("test")
public class PricesCalculatorTest {

	@Autowired
	private PricesCalculator calculator;
	
	/**
	 * Tests when price calculation is ok and all flights have 2 bags
	 * @throws Exception
	 */
	@Test
	public void whenPriceCalculationIsOkAndHas2Bags() {
		assertThat(calculator).isNotNull();
		
		// Creates dummy data
		List<FlightData> flightsData = List.of(
			new FlightData(BigDecimal.valueOf(100.0), "LIS", "OPO", BigDecimal.valueOf(20.0), BigDecimal.valueOf(15.0)),
			new FlightData(BigDecimal.valueOf(120.0), "LIS", "OPO", BigDecimal.valueOf(26.0), BigDecimal.valueOf(19.0)));
		
		Flight flight = new Flight();
		
		calculator.calculatePrices(flightsData, flight, "GBR", BigDecimal.valueOf(0.9));
		
		assertThat(flight.getFlightPrices()).isNotNull();
		
		// Calculate average flight prices
		Price priceGBR = flight.getFlightPriceByCurrency("GBR");
		assertThat(priceGBR).isNotNull();
		assertThat(priceGBR.getValue()).isEqualTo(NumberUtils.toBigDecimalWithScale(110.0));
		
		Price priceEUR = flight.getFlightPriceByCurrency("EUR");
		assertThat(priceEUR).isNotNull();
		assertThat(priceEUR.getValue()).isEqualTo(NumberUtils.toBigDecimalWithScale(99.0));
		
		
		// Calculate average bag #1 prices
		assertThat(flight.getBag1prices()).isNotNull();
		
		priceGBR = flight.getBag1PriceByCurrency("GBR");
		assertThat(priceGBR).isNotNull();
		assertThat(priceGBR.getValue()).isEqualTo(NumberUtils.toBigDecimalWithScale(23.0));
		
		priceEUR = flight.getBag1PriceByCurrency("EUR");
		assertThat(priceEUR).isNotNull();
		assertThat(priceEUR.getValue()).isEqualTo(NumberUtils.toBigDecimalWithScale(20.7));
		
		// Calculate average bag #2 prices
		assertThat(flight.getBag2prices()).isNotNull();
		
		priceGBR = flight.getBag2PriceByCurrency("GBR");
		assertThat(priceGBR).isNotNull();
		assertThat(priceGBR.getValue()).isEqualTo(NumberUtils.toBigDecimalWithScale(17.0));
		
		priceEUR = flight.getBag2PriceByCurrency("EUR");
		assertThat(priceEUR).isNotNull();
		assertThat(priceEUR.getValue()).isEqualTo(NumberUtils.toBigDecimalWithScale(15.3));
	}

	/**
	 * Tests when price calculation is ok and at least one flight has 1 bag
	 * @throws Exception
	 */
	@Test
	public void whenPriceCalculationIsOkAndHas1Bag() {
		assertThat(calculator).isNotNull();
		
		// Creates dummy data
		List<FlightData> flightsData = List.of(
			new FlightData(BigDecimal.valueOf(100.0), "LIS", "OPO", BigDecimal.valueOf(20.0), BigDecimal.valueOf(15.0)),
			new FlightData(BigDecimal.valueOf(120.0), "LIS", "OPO", BigDecimal.valueOf(26.0), null),
			new FlightData(BigDecimal.valueOf(120.0), "LIS", "OPO", BigDecimal.valueOf(20.0), BigDecimal.valueOf(15.0)));
		
		Flight flight = new Flight();
		
		calculator.calculatePrices(flightsData, flight, "GBR", BigDecimal.valueOf(0.9));
		
		assertThat(flight.getFlightPrices()).isNotNull();
		
		// Calculate average bag #2 prices (should not consider bag #2)
		assertThat(flight.getBag2prices()).isNotNull();
		
		Price priceGBR = flight.getBag2PriceByCurrency("GBR");
		assertThat(priceGBR).isNotNull();
		assertThat(priceGBR.getValue()).isEqualTo(NumberUtils.toBigDecimalWithScale(15.0));
		
		Price priceEUR = flight.getBag2PriceByCurrency("EUR");
		assertThat(priceEUR).isNotNull();
		assertThat(priceEUR.getValue()).isEqualTo(NumberUtils.toBigDecimalWithScale(13.5));
	}
}
