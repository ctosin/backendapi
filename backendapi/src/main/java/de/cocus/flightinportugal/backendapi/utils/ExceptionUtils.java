package de.cocus.flightinportugal.backendapi.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Utility class for exception handling
 * @author Carlos
 */
public class ExceptionUtils {

	/**
	 * Extracts the exception stacktrace to a String
	 * @param t Exception
	 * @return
	 */
	public static String stackTraceToString(Throwable t) {
		StringWriter sw = new StringWriter();
		t.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}
}
