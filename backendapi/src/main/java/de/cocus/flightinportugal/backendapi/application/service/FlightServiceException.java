package de.cocus.flightinportugal.backendapi.application.service;

/**
 * Exception thrown by {@link FlightService}
 * @author Carlos
 */
@SuppressWarnings("serial")
public class FlightServiceException extends Exception {

	/**
	 * The exception can carry errors in JSON format
	 */
	private String jsonError;
	
	public FlightServiceException(String message) {
		super(message);
	}
	
	public FlightServiceException(String message, String jsonError) {
		super(message);
		this.jsonError = jsonError;
	}
	
	public String getJsonError() {
		return jsonError;
	}
}
