package de.cocus.flightinportugal.backendapi.infrastructure.web.config;

import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

/**
 * Class to configure Spring controllers
 * @author Carlos
 */
@ControllerAdvice
public class BindingControllerAdvice {

	/**
	 * Setup the direct field access in binder. This is necessary so Spring can inject query parameters
	 * during REST requests, without the need of definining setter methods to properties
	 * @param binder
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.initDirectFieldAccess();
	}
}