package de.cocus.flightinportugal.backendapi.infrastructure.web.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.cocus.flightinportugal.backendapi.application.service.FlightService;
import de.cocus.flightinportugal.backendapi.application.service.FlightServiceException;
import de.cocus.flightinportugal.backendapi.application.service.FlightServiceRequest;
import de.cocus.flightinportugal.backendapi.application.service.LogService;
import de.cocus.flightinportugal.backendapi.domain.model.flight.Flight;
import de.cocus.flightinportugal.backendapi.domain.model.log.RequestLog;
import de.cocus.flightinportugal.backendapi.infrastructure.web.rest.RESTResponse;
import de.cocus.flightinportugal.backendapi.infrastructure.web.rest.RESTResponse.Error;
import de.cocus.flightinportugal.backendapi.infrastructure.webservice.KiwiServiceProxy;
import de.cocus.flightinportugal.backendapi.utils.ExceptionUtils;

/**
 * REST controller
 * @author Carlos
 */
@RestController
@RequestMapping(path = "/flight")
public class FlightSearchController {
	private static final Logger logger = LoggerFactory.getLogger(KiwiServiceProxy.class);
	
	@Autowired
	private FlightService flightService;
	
	@Autowired
	private LogService logService;
	
	@Autowired
	private ObjectMapper mapper;

	/**
	 * Calculates flights average prices
	 * @param request
	 * @param servletRequest
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "avg", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RESTResponse> listFlightsAverages(@Valid FlightServiceRequest request, HttpServletRequest servletRequest) throws Exception {
		logger.debug("Receiving a new request. Data: " + request);
		
		request.setIpAddress(servletRequest.getRemoteAddr());		
		Flight response = flightService.findFlightPriceAverages(request);
		
		logger.debug("Response is ready!");
		
		return ResponseEntity.ok(RESTResponse.success(response));
	}
	
	@GetMapping(path = "/logs", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RESTResponse> listAllRequestLogs() throws Exception {
		logger.debug("Listing request logs...");
		
		List<RequestLog> logs = logService.listAllRequestLogs();
		
		logger.debug("{} log(s) found", logs.size());
		
		return ResponseEntity.ok(RESTResponse.success(logs));
	}
	
	@DeleteMapping(path = "/logs", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RESTResponse> deleteAllRequestLogs() throws Exception {
		logger.debug("Deleting all the request logs...");
		
		logService.deleteAllRequestLogs();
		
		logger.debug("Logs deleted!");
		
		return ResponseEntity.ok(RESTResponse.success());
	}
	
	/**
	 * Exception handler for {@link BindException} type, thrown by Bean Validator
	 * @param e
	 * @return
	 */
	@ExceptionHandler(BindException.class)
	protected ResponseEntity<Object> handleException(BindException e) {
		logger.debug("Handling a validation exception: " + ExceptionUtils.stackTraceToString(e));
		
		Error errorResult = RESTResponse.error();
		e.getBindingResult().getAllErrors().forEach(error -> errorResult.addErrorMessage(error.getDefaultMessage()));
		return ResponseEntity.badRequest().body(errorResult.buildResult());
	}
	
	/**
	 * Exception handler for {@link FlightServiceException} type, thrown by {@link FlightService}
	 * @param fse
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@ExceptionHandler(FlightServiceException.class)
	protected ResponseEntity<Object> handleException(FlightServiceException fse) {
		logger.debug("Handling a flight service exception: " + ExceptionUtils.stackTraceToString(fse));
		
		Error errorResult = RESTResponse.error();

		try {
			Map<String, Object> errorMap = mapper.readValue(fse.getJsonError(), Map.class);
			
			List<Map<String, Object>> errors =  (List<Map<String, Object>>) errorMap.get("message");
			
			errors.forEach(map -> {
				List<String> errorMessages = (List<String>) map.get("errors");
				errorMessages.forEach(errorMessage -> errorResult.addErrorMessage(errorMessage));
			});
			
			return ResponseEntity.badRequest().body(errorResult.buildResult());
		
		} catch (JsonProcessingException jpe) {
			return handleException(jpe);
		}
	}
	
	/**
	 * Exception handler for any other exception type
	 * @param e
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	protected ResponseEntity<Object> handleException(Exception e) {
		logger.debug("Handling a generic exception: " + ExceptionUtils.stackTraceToString(e));
		return ResponseEntity
				.badRequest()
				.body(
						RESTResponse.error()
						.addErrorMessage("Unknown error: " + e.getMessage())
						.buildResult()
				);
	}
}
