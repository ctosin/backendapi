package de.cocus.flightinportugal.backendapi.application.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.cocus.flightinportugal.backendapi.domain.model.flight.Flight;
import de.cocus.flightinportugal.backendapi.domain.service.flight.PricesCalculator;
import de.cocus.flightinportugal.backendapi.infrastructure.flight.Factories;
import de.cocus.flightinportugal.backendapi.infrastructure.webservice.KiwiFlightSearchException;
import de.cocus.flightinportugal.backendapi.infrastructure.webservice.KiwiFlightSearchRequest;
import de.cocus.flightinportugal.backendapi.infrastructure.webservice.KiwiFlightSearchResponse;
import de.cocus.flightinportugal.backendapi.infrastructure.webservice.KiwiServiceProxy;

/**
 * Flight service, which fetch flights and calculate the average prices
 * @author Carlos
 */
@Service
public class FlightService {

	@Autowired
	private KiwiServiceProxy proxy;
	
	@Autowired
	private PricesCalculator pricesCalculator;
	
	@Autowired
	private LogService logService;
	
	/**
	 * Find a flight with average prices (flight and bags) based on the request data
	 * @param request
	 * @return
	 * @throws FlightServiceException
	 */
	public Flight findFlightPriceAverages(FlightServiceRequest request) throws FlightServiceException {
		try {
			// Prepares a request to the Kiwi flight search web service
			KiwiFlightSearchRequest kiwiRequest = Factories.createKiwiFlightSearchRequest(request);
			
			// Calls the web service
			KiwiFlightSearchResponse kiwiResponse = proxy.call(kiwiRequest);
			
			// Prepares the response
			Flight flight = Factories.buildFlight(request, kiwiResponse);
			
			// Calculate averages
			pricesCalculator.calculatePrices(kiwiResponse.getFlights(), flight, kiwiResponse.getCurrency(), kiwiResponse.getCurrencyRate());
			
			// Log the request
			logService.logRequest(request.getIpAddress(), request, flight, kiwiResponse.isCached());
			
			return flight;
		
		} catch (KiwiFlightSearchException e) {
			throw new FlightServiceException("Error from Kiwi web service", e.getJsonError());
		}
	}
}
