package de.cocus.flightinportugal.backendapi.infrastructure.webservice;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

/**
 * Caches the Kiwi flight service web service call
 * @author Carlos
 */
public class KiwiServiceCachedProxy extends KiwiServiceProxy {
	private static final Logger logger = LoggerFactory.getLogger(KiwiServiceCachedProxy.class);
	
	/**
	 * Amount of time before cache is expired (in seconds)
	 */
	@Value("${app.kiwi.cachedProxy.expirationTimeInSecs}")
	private int cacheExpirationTimeInSecs;

	/**
	 * Map to store cached data
	 */
	private Map<KiwiFlightSearchRequest, CachedResult> cache = new HashMap<>();
	
	@Override
	public KiwiFlightSearchResponse call(KiwiFlightSearchRequest request) throws KiwiFlightSearchException {
		logger.debug("Trying to get Kiwi data from the cache");
		
		// Tries to find data in the cache
		CachedResult cachedResult = cache.get(request);
		
		// If not found, or cache expired, get data again
		if (cachedResult == null || cachedResult.isExpired()) {
			logger.debug("Data not found or cache expired. Doing a real request");
			KiwiFlightSearchResponse result = super.call(request);
			
			// Put new data in cache
			cachedResult = new CachedResult(result, System.currentTimeMillis());
			cachedResult.getResult().setCached(false);
			cache.put(request, cachedResult);
		
		} else {
			logger.debug("Cached data found. Returning it");
			cachedResult.getResult().setCached(true);
		}
		
		return cachedResult.getResult();
	}
	
	/**
	 * Represents a cache, i.e. groups the web service response with a timestamp
	 * @author Carlos
	 */
	private class CachedResult {
		/**
		 * Web service response
		 */
		private KiwiFlightSearchResponse result;
		
		/**
		 * Request timestamp 
		 */
		private long timestamp;
		
		public CachedResult(KiwiFlightSearchResponse result, long timestamp) {
			this.result = result;
			this.timestamp = timestamp;
		}

		public KiwiFlightSearchResponse getResult() {
			return result;
		}

		/**
		 * Detects if the cache is expired
		 * @return
		 */
		public boolean isExpired() {
			return System.currentTimeMillis() - timestamp > cacheExpirationTimeInSecs * 1000;
		}
	}
}
