package de.cocus.flightinportugal.backendapi.domain.model.log;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * Entity representing an API request log
 * @author Carlos
 */
@Entity
public class RequestLog {

	/**
	 * Request ID (auto-generated)
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * Time of the log
	 */
	@NotNull
	@Column(name = "log_time")
	private LocalDateTime logTime;
	
	/**
	 * Source IP address
	 */
	@Column(name = "ip_address", length = 30)
	private String ipAddress;
	
	/**
	 * Request data (JSON format)
	 */
	@NotNull
	@Column(length = 5096)
	private String requestData;
	
	/**
	 * Response data (JSON format)
	 */
	@NotNull
	@Column(length = 5096)
	private String responseData;
	
	/**
	 * Whether the response was retrieved from the cache or not
	 */
	@NotNull
	private Boolean cachedResponse;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getLogTime() {
		return logTime;
	}

	public void setLogTime(LocalDateTime logTime) {
		this.logTime = logTime;
	}
	
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getRequestData() {
		return requestData;
	}

	public void setRequestData(String requestData) {
		this.requestData = requestData;
	}

	public String getResponseData() {
		return responseData;
	}

	public void setResponseData(String responseData) {
		this.responseData = responseData;
	}

	public Boolean getCachedResponse() {
		return cachedResponse;
	}

	public void setCachedResponse(Boolean cachedResponse) {
		this.cachedResponse = cachedResponse;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RequestLog other = (RequestLog) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RequestLog [id=" + id + ", logTime=" + logTime + ", requestData=" + requestData + ", responseData="
				+ responseData + ", cachedResponse=" + cachedResponse + "]";
	}
}
