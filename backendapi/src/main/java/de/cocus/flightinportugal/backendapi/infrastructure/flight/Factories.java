package de.cocus.flightinportugal.backendapi.infrastructure.flight;

import de.cocus.flightinportugal.backendapi.application.service.FlightServiceRequest;
import de.cocus.flightinportugal.backendapi.domain.model.flight.Flight;
import de.cocus.flightinportugal.backendapi.infrastructure.webservice.KiwiFlightSearchRequest;
import de.cocus.flightinportugal.backendapi.infrastructure.webservice.KiwiFlightSearchResponse;

/**
 * Factory class with static methods do create instances of several types
 * @author Carlos
 */
public class Factories {

	/**
	 * Creates a {@link Flight} object from a {@link FlightServiceRequest} and a {@link KiwiFlightSearchResponse} object
	 * @param request
	 * @param kiwiResponse
	 * @return
	 */
	public static Flight buildFlight(FlightServiceRequest request, KiwiFlightSearchResponse kiwiResponse) {
		Flight flight = new Flight();
		
		flight.setDateFrom(request.getDateFrom());
		flight.setDateTo(request.getDateTo());
		
		flight.setAirportFrom(request.getAirportFrom());
		flight.setAirportTo(request.getAirportTo());
		
		return flight;
	}
	
	/**
	 * Creates a {@link KiwiFlightSearchRequest} object from a {@link FlightServiceRequest} object
	 * @param flightServiceRequest
	 * @return
	 */
	public static KiwiFlightSearchRequest createKiwiFlightSearchRequest(FlightServiceRequest flightServiceRequest) {
		return new KiwiFlightSearchRequest(
				flightServiceRequest.getAirportFrom(),
				flightServiceRequest.getAirportTo(),
				flightServiceRequest.getDateFrom(), flightServiceRequest.getDateTo(),
				flightServiceRequest.getCurrency());
				
	}
}
