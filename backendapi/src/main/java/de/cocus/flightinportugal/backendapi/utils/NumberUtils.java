package de.cocus.flightinportugal.backendapi.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Number related utility class
 * @author Carlos
 */
public class NumberUtils {

	/**
	 * Decimal precision used in currencies
	 */
	private static final int DEFAULT_DECIMAL_PRECISION = 2;
	
	/**
	 * Converts a double to a scaled BigDecimal
	 * @param value Converted value
	 * @return
	 */
	public static BigDecimal toBigDecimalWithScale(double value) {
		return BigDecimal.valueOf(value).setScale(DEFAULT_DECIMAL_PRECISION, RoundingMode.HALF_EVEN);
	}
	
	/**
	 * Converts a BigDecimal to a scaled BigDecimal
	 * @param value Converted value or <code>BigDecimal.ZERO</code> if value is <code>null</code>
	 * @return
	 */
	public static BigDecimal toBigDecimalWithScale(BigDecimal value) {
		if (value == null) {
			return BigDecimal.ZERO;
		}
		
		return value.setScale(DEFAULT_DECIMAL_PRECISION, RoundingMode.HALF_EVEN);
	}
}
