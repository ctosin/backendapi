package de.cocus.flightinportugal.backendapi.utils;

import java.time.format.DateTimeFormatter;

/**
 * Application constants
 * @author Carlos
 */
public class Constants {
	
	/**
	 * Default date pattern
	 */
	public static final String DATE_PATTERN = "dd/MM/yyyy";
	
	/**
	 * Default date formatter
	 */
	public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_PATTERN);
	
	/**
	 * EUR currency constant
	 */
	public static final String CURRENCY_EUR = "EUR";
}
