package de.cocus.flightinportugal.backendapi.domain.model.log;

import org.springframework.data.repository.CrudRepository;

/**
 * Repository associated to {@link RequestLog} entity
 * @author Carlos
 */
public interface RequestLogRepository extends CrudRepository<RequestLog, Long> {
}
