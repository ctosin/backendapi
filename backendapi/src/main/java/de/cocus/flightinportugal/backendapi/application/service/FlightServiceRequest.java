package de.cocus.flightinportugal.backendapi.application.service;

import java.time.LocalDate;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import de.cocus.flightinportugal.backendapi.utils.Constants;

/**
 * Request data for {@link FlightService}
 * @author Carlos
 */
public class FlightServiceRequest {
	
	/**
	 * Departure airport
	 */
	@NotNull(message = "The from airport is mandatory")
	@Length(min = 3, max = 3, message = "The from aiport must have 3 characters")
	private String airportFrom;
	
	/**
	 * Arrival airport
	 */
	@NotNull(message = "The to airport is mandatory")
	@Length(min = 3, max = 3, message = "The to aiport must have 3 characters")
	private String airportTo;
	
	/**
	 * Departure date
	 */
	@DateTimeFormat(pattern = Constants.DATE_PATTERN)
	@NotNull(message = "The from date is mandatory")
	@Future(message = "The from date must be in the future")
	private LocalDate dateFrom;
	
	/**
	 * Arrival date
	 */
	@DateTimeFormat(pattern = Constants.DATE_PATTERN)
	@NotNull(message = "The to date is mandatory")
	@Future(message = "The to date must be in the future")
	private LocalDate dateTo;
	
	/**
	 * Currency to be considered when calculating prices
	 */
	@NotNull(message =  "The currency is mandatory")
	private String currency;
	
	/**
	 * Request IP address
	 */
	private String ipAddress;

	public FlightServiceRequest(
			@NotNull(message = "The from airport is mandatory") @Length(min = 3, max = 3, message = "The from aiport must have 3 characters") String airportFrom,
			@NotNull(message = "The to airport is mandatory") @Length(min = 3, max = 3, message = "The to aiport must have 3 characters") String airportTo,
			@NotNull(message = "The from date is mandatory") @Future(message = "The from date must be in the future") LocalDate dateFrom,
			@NotNull(message = "The to date is mandatory") @Future(message = "The to date must be in the future") LocalDate dateTo,
			@NotNull(message = "The currency is mandatory") String currency, String ipAddress) {
		this.airportFrom = airportFrom;
		this.airportTo = airportTo;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.currency = currency;
		this.ipAddress = ipAddress;
	}

	public String getAirportFrom() {
		return airportFrom;
	}

	public String getAirportTo() {
		return airportTo;
	}

	public LocalDate getDateFrom() {
		return dateFrom;
	}

	public LocalDate getDateTo() {
		return dateTo;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Override
	public String toString() {
		return "FlightSearchRequest [airportFrom=" + airportFrom + ", airportTo=" + airportTo + ", dateFrom=" + dateFrom
				+ ", dateTo=" + dateTo + ", currency=" + currency + ", ipAddress=" + ipAddress + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((airportFrom == null) ? 0 : airportFrom.hashCode());
		result = prime * result + ((airportTo == null) ? 0 : airportTo.hashCode());
		result = prime * result + ((currency == null) ? 0 : currency.hashCode());
		result = prime * result + ((dateFrom == null) ? 0 : dateFrom.hashCode());
		result = prime * result + ((dateTo == null) ? 0 : dateTo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlightServiceRequest other = (FlightServiceRequest) obj;
		if (airportFrom == null) {
			if (other.airportFrom != null)
				return false;
		} else if (!airportFrom.equals(other.airportFrom))
			return false;
		if (airportTo == null) {
			if (other.airportTo != null)
				return false;
		} else if (!airportTo.equals(other.airportTo))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (dateFrom == null) {
			if (other.dateFrom != null)
				return false;
		} else if (!dateFrom.equals(other.dateFrom))
			return false;
		if (dateTo == null) {
			if (other.dateTo != null)
				return false;
		} else if (!dateTo.equals(other.dateTo))
			return false;
		return true;
	}
}
