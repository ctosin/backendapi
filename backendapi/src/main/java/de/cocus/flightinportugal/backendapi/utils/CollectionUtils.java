package de.cocus.flightinportugal.backendapi.utils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Utility class for collections
 * @author Carlos
 *
 */
public class CollectionUtils {

	/**
	 * Converts an iterable to a list (ArrayList)
	 * @param <T>
	 * @param iterable
	 * @return
	 */
	public static <T> List<T> convertIterableToList(Iterable<T> iterable) {
		return StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());
	}
}
