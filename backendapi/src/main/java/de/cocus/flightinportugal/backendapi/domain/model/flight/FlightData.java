package de.cocus.flightinportugal.backendapi.domain.model.flight;

import java.math.BigDecimal;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Flight data returned from the Kiwi flight search web service
 * @author Carlos
 */
public class FlightData {
	
	/**
	 * Flight price
	 */
	@JsonProperty("price")
	private BigDecimal price;
	
	/**
	 * Departure airport (IATA code)
	 */
	@JsonProperty("flyFrom")
	private String airportFrom;
	
	/**
	 * Destination airport (IATA code)
	 */
	@JsonProperty("flyTo")
	private String airportTo;
	
	/**
	 * Price for Bag #1
	 */
	private BigDecimal bag1Price;
	
	/**
	 * Price for Bag #2 (can be null)
	 */
	private BigDecimal bag2Price;
	
	public FlightData() {
	}
	
	public FlightData(BigDecimal price, String airportFrom, String airportTo, BigDecimal bag1Price, BigDecimal bag2Price) {
		this.price = price;
		this.airportFrom = airportFrom;
		this.airportTo = airportTo;
		this.bag1Price = bag1Price;
		this.bag2Price = bag2Price;
	}

	@JsonProperty("bags_price")
	private void unpackBagsPrice(Map<String, BigDecimal> bagsPrice) {
		this.bag1Price = bagsPrice.get("1");
		this.bag2Price = bagsPrice.get("2");
	}
	
	public BigDecimal getPrice() {
		return price;
	}

	public BigDecimal getBag1Price() {
		return bag1Price;
	}
	
	public BigDecimal getBag2Price() {
		return bag2Price;
	}
	
	public String getAirportFrom() {
		return airportFrom;
	}
	
	public String getAirportTo() {
		return airportTo;
	}

	@Override
	public String toString() {
		return "Flight [price=" + price + ", airportFrom=" + airportFrom + ", airportTo=" + airportTo + ", bag1Price="
				+ bag1Price + ", bag2Price=" + bag2Price + "]";
	}
}
