package de.cocus.flightinportugal.backendapi.infrastructure.webservice;

/**
 * Exception for Kiwi web service requests
 * @author Carlos
 */
@SuppressWarnings("serial")
public class KiwiFlightSearchException extends Exception {

	public KiwiFlightSearchException(String jsonError, Throwable cause) {
		super(jsonError, cause);
	}
	
	public String getJsonError() {
		return getMessage();
	}
}
