package de.cocus.flightinportugal.backendapi.application.service;

import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.cocus.flightinportugal.backendapi.domain.model.flight.Flight;
import de.cocus.flightinportugal.backendapi.domain.model.log.RequestLog;
import de.cocus.flightinportugal.backendapi.domain.model.log.RequestLogRepository;
import de.cocus.flightinportugal.backendapi.utils.CollectionUtils;

/**
 * Log Service
 * @author Carlos
 */
@Service
public class LogService {
	private static final Logger logger = LoggerFactory.getLogger(LogService.class);

	@Autowired
	private RequestLogRepository repository;
	
	public RequestLog logRequest(String ipAddress, FlightServiceRequest request, Flight flight, boolean cachedResponse) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			
			RequestLog log = new RequestLog();
			log.setLogTime(LocalDateTime.now());
			log.setIpAddress(ipAddress);
			log.setRequestData(mapper.writeValueAsString(request));
			log.setResponseData(mapper.writeValueAsString(flight));
			log.setCachedResponse(cachedResponse);
			
			repository.save(log);
			
			logger.debug("Logging request from host {}. Data is: {}", ipAddress, log);
			
			return log;
		
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			// In case of error, ignore the logging. Here is a good place to send an e-mail or something :)
			
			return null;
		}
	}
	
	/**
	 * Lists all available request logs
	 * @return
	 */
	public List<RequestLog> listAllRequestLogs() {
		return CollectionUtils.convertIterableToList(repository.findAll());
	}
	
	/**
	 * Deletes all available request logs
	 */
	public void deleteAllRequestLogs() {
		repository.deleteAll();
	}
}
