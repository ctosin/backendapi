package de.cocus.flightinportugal.backendapi.infrastructure.webservice;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.cocus.flightinportugal.backendapi.domain.model.flight.FlightData;

/**
 * Response object from the Kiwi flight search airport
 * @author Carlos
 */
public class KiwiFlightSearchResponse {

	/**
	 * Flight data returned from the web service
	 */
	@JsonProperty("data")
	private List<FlightData> flights;
	
	/**
	 * Prices currency
	 */
	@JsonProperty("currency")
	private String currency;
	
	/**
	 * Currency rate to convert current currency to EUR
	 */
	@JsonProperty("currency_rate")
	private BigDecimal currencyRate;
	
	/**
	 * Identifies whether the response was retrieved from the cache or not 
	 */
	private transient boolean cached;
	
	
	public List<FlightData> getFlights() {
		return flights;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public BigDecimal getCurrencyRate() {
		return currencyRate;
	}
	
	public void setCached(boolean cached) {
		this.cached = cached;
	}
	
	public boolean isCached() {
		return cached;
	}

	@Override
	public String toString() {
		return "KiwiFlightSearchResponse [flights=" + flights + ", currency=" + currency + ", currencyRate="
				+ currencyRate + "]";
	}
}
