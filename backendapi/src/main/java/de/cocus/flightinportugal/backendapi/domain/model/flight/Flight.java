package de.cocus.flightinportugal.backendapi.domain.model.flight;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.cocus.flightinportugal.backendapi.utils.Constants;

/**
 * Represents a flight with average prices
 * @author Carlos
 */
public class Flight {

	/**
	 * Departure date
	 */
	@JsonProperty("date_from")
	@JsonFormat(pattern = Constants.DATE_PATTERN)
	private LocalDate dateFrom;

	/**
	 * Arrival date
	 */
	@JsonProperty("date_to")
	@JsonFormat(pattern = Constants.DATE_PATTERN)
	private LocalDate dateTo;

	/**
	 * Departure airport (IATA code)
	 */
	@JsonProperty("airport_from")
	private String airportFrom;

	/**
	 * Arrival airport (IATA code)
	 */
	@JsonProperty("airport_to")
	private String airportTo;

	/**
	 * Average flight prices
	 */
	@JsonProperty("flight_price_avg")
	private List<Price> flightPrices = new ArrayList<Price>(2);
	
	/**
	 * Map of flight prices
	 */
	private Map<String, Price> flightPricesMap = new LinkedHashMap<>();

	/**
	 * Average Bag #1 prices
	 */
	@JsonProperty("bag1_price_avg")
	private List<Price> bag1prices = new ArrayList<>(2);
	
	/**
	 * Map of bag #1 prices
	 */
	private Map<String, Price> bag1PricesMap = new LinkedHashMap<>();

	/**
	 * Average Bag #2 prices
	 */
	@JsonProperty("bag2_price_avg")
	private List<Price> bag2prices = new ArrayList<>(2);
	
	/**
	 * Map of bag #2 prices
	 */
	private Map<String, Price> bag2PricesMap = new LinkedHashMap<>();

	
	public LocalDate getDateFrom() {
		return dateFrom;
	}

	public LocalDate getDateTo() {
		return dateTo;
	}

	public List<Price> getBag1prices() {
		return bag1prices;
	}

	public List<Price> getBag2prices() {
		return bag2prices;
	}

	public String getAirportFrom() {
		return airportFrom;
	}

	public String getAirportTo() {
		return airportTo;
	}

	public List<Price> getFlightPrices() {
		return flightPrices;
	}

	public void addFlightPrice(Price price) {
		this.flightPrices.add(price);
		this.flightPricesMap.put(price.getCurrency(), price);
	}
	
	public Price getFlightPriceByCurrency(String currency) {
		return this.flightPricesMap.get(currency);
	}

	public void setDateFrom(LocalDate dateFrom) {
		this.dateFrom = dateFrom;
	}

	public void setDateTo(LocalDate dateTo) {
		this.dateTo = dateTo;
	}

	public void setAirportFrom(String airportFrom) {
		this.airportFrom = airportFrom;
	}

	public void setAirportTo(String airportTo) {
		this.airportTo = airportTo;
	}

	public void addBag1price(Price price) {
		this.bag1prices.add(price);
		this.bag1PricesMap.put(price.getCurrency(), price);
	}
	
	public Price getBag1PriceByCurrency(String currency) {
		return this.bag1PricesMap.get(currency);
	}
	
	public void addBag2price(Price price) {
		this.bag2prices.add(price);
		this.bag2PricesMap.put(price.getCurrency(), price);
	}
	
	public Price getBag2PriceByCurrency(String currency) {
		return this.bag2PricesMap.get(currency);
	}

	@Override
	public String toString() {
		return "Flight [dateFrom=" + dateFrom + ", dateTo=" + dateTo + ", airportFrom=" + airportFrom + ", airportTo="
				+ airportTo + ", flightPrices=" + flightPrices + ", bag1prices=" + bag1prices + ", bag2prices="
				+ bag2prices + "]";
	}
}
