package de.cocus.flightinportugal.backendapi.infrastructure.webservice;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import de.cocus.flightinportugal.backendapi.utils.Constants;

/**
 * Redirects the call to the Kiwi flight search web service
 * 
 * @author Carlos
 */
@Service
public class KiwiServiceProxy {
	private static final Logger logger = LoggerFactory.getLogger(KiwiServiceProxy.class);

	@Autowired
	private RestTemplate rt;

	@Value("${app.kiwiws.endpoint}")
	private String endpoint;

	/**
	 * Calls the web service
	 * 
	 * @param request Request parameters
	 * @return
	 * @throws KiwiFlightSearchException Thrown in case of an error response
	 */
	public KiwiFlightSearchResponse call(KiwiFlightSearchRequest request) throws KiwiFlightSearchException {
		try {
			// Builds the URI
			URI uri = UriComponentsBuilder
					.fromHttpUrl(endpoint)
					.queryParam("fly_from", request.getAirportFrom())
					.queryParam("fly_to", request.getAirportTo())
					.queryParam("date_from", Constants.DATE_FORMATTER.format(request.getDateFrom()))
					.queryParam("date_to", Constants.DATE_FORMATTER.format(request.getDateTo()))
					.queryParam("curr", request.getCurrency()).queryParam("partner", "picky")
					.build().encode().toUri();

			logger.debug("Calling Kiwi web service URI at " + uri);
			
			ResponseEntity<KiwiFlightSearchResponse> response = rt.getForEntity(uri, KiwiFlightSearchResponse.class);

			logger.debug("Kiwi web service responded: " + response);

			return response.getBody();

		} catch (HttpClientErrorException e) {
			throw new KiwiFlightSearchException(e.getResponseBodyAsString(), e);
		}
	}
}
