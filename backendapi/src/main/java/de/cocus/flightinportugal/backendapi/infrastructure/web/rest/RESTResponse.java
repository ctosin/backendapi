package de.cocus.flightinportugal.backendapi.infrastructure.web.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents a REST response to the backend API clients
 * @author Carlos
 */
@JsonInclude(Include.NON_NULL)
public class RESTResponse {

	/**
	 * Response HTTP code
	 */
	@JsonProperty("code")
	private int code;
	
	/**
	 * Flag indicating if the request was successful
	 */
	@JsonProperty("success")
	private boolean success;
	
	/**
	 * List of error messages (only in case of error)
	 */
	@JsonProperty("errors")
	private List<String> errorMessages;
	
	/**
	 * Response data (only in case of success)
	 */
	@JsonProperty("data")
	private Object data;
	
	
	private RESTResponse() {
	}
	
	/**
	 * Factory method to create a success response
	 * @param data Response data
	 * @return
	 */
	public static RESTResponse success(Object data) {
		RESTResponse result = new RESTResponse();
		result.code = HttpServletResponse.SC_OK;
		result.success = true;
		result.data = data;
		return result;
	}
	
	/**
	 * Factory method to create a success response with no response body
	 * @param data Response data
	 * @return
	 */
	public static RESTResponse success() {
		return success(null);
	}
	
	/**
	 * Factory method to create an error response
	 * @return An {@link Error} object which can have errors added to it
	 */
	public static Error error() {
		RESTResponse result = new RESTResponse();
		result.code = HttpServletResponse.SC_BAD_REQUEST;
		result.success = false;
		return new Error(result);
	}
	
	/**
	 * Represents an error to send to the client
	 * @author Carlos
	 */
	public static class Error {
		private RESTResponse result;
		
		private Error(RESTResponse result) {
			this.result = result;
		}
		
		/**
		 * Adds an error message to the response
		 * @param errorMessage Error to be added
		 * @return
		 */
		public Error addErrorMessage(String errorMessage) {
			if (result.errorMessages == null) {
				result.errorMessages = new ArrayList<>();
			}
			
			result.errorMessages.add(errorMessage);
			return this;
		}
		
		/**
		 * Returns the response object
		 * @return
		 */
		public RESTResponse buildResult() {
			return result;
		}
	}
}
