package de.cocus.flightinportugal.backendapi;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

import de.cocus.flightinportugal.backendapi.infrastructure.webservice.KiwiServiceCachedProxy;
import de.cocus.flightinportugal.backendapi.infrastructure.webservice.KiwiServiceProxy;

@SpringBootApplication
public class BackendapiApplication {
	private static final Logger logger = org.slf4j.LoggerFactory.getLogger(BackendapiApplication.class);
	
	/**
	 * Property to define if the KiwiServiceProxy will cache responses (0 means no cache)
	 */
	@Value("${app.kiwi.cachedProxy.expirationTimeInSecs}")
	private int cacheExpirationTime;

	/**
	 * Entry point
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(BackendapiApplication.class, args);
	}

	/**
	 * Creates a Rest Template object
	 * @param builder
	 * @return
	 */
	@Bean
	public RestTemplate createRestTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
	
	/**
	 * Creates a proxy do the KiwiService. Depending on 'app.kiwi.cachedProxy' property, the proxy may cache responses.
	 * @return
	 */
	@Bean
	@Primary
	public KiwiServiceProxy createKiwiServiceProxy() {
		KiwiServiceProxy service = cacheExpirationTime > 0 ? new KiwiServiceCachedProxy() : new KiwiServiceProxy();
		logger.debug("{} instance is of type {}", KiwiServiceProxy.class.getSimpleName(), service.getClass().getSimpleName());
		return service;
	}
}
