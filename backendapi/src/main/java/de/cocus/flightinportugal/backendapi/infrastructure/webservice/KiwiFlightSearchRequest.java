package de.cocus.flightinportugal.backendapi.infrastructure.webservice;

import java.time.LocalDate;

/**
 * Request object for the Kiwi flight search web service
 * @author Carlos
 */
public class KiwiFlightSearchRequest {
	
	/**
	 * Departure airport (IATA code)
	 */
	private String airportFrom;
	
	/**
	 * Destination airport (IATA code)
	 */
	private String airportTo;
	
	/**
	 * Departure date
	 */
	private LocalDate dateFrom;
	
	/**
	 * Arrival date
	 */
	private LocalDate dateTo;
	
	/**
	 * Currency to be used (3-letter currency code)
	 */
	private String currency;
	
	public KiwiFlightSearchRequest(String airportFrom, String airportTo, LocalDate dateFrom, LocalDate dateTo, String currency) {
		this.airportFrom = airportFrom;
		this.airportTo = airportTo;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.currency = currency;
	}

	public String getAirportFrom() {
		return airportFrom;
	}

	public String getAirportTo() {
		return airportTo;
	}

	public LocalDate getDateFrom() {
		return dateFrom;
	}

	public LocalDate getDateTo() {
		return dateTo;
	}

	public String getCurrency() {
		return currency;
	}

	@Override
	public String toString() {
		return "KiwiFlightSearchRequest [airportFrom=" + airportFrom + ", airportTo=" + airportTo + ", dateFrom="
				+ dateFrom + ", dateTo=" + dateTo + ", currency=" + currency + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((airportFrom == null) ? 0 : airportFrom.hashCode());
		result = prime * result + ((airportTo == null) ? 0 : airportTo.hashCode());
		result = prime * result + ((currency == null) ? 0 : currency.hashCode());
		result = prime * result + ((dateFrom == null) ? 0 : dateFrom.hashCode());
		result = prime * result + ((dateTo == null) ? 0 : dateTo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KiwiFlightSearchRequest other = (KiwiFlightSearchRequest) obj;
		if (airportFrom == null) {
			if (other.airportFrom != null)
				return false;
		} else if (!airportFrom.equals(other.airportFrom))
			return false;
		if (airportTo == null) {
			if (other.airportTo != null)
				return false;
		} else if (!airportTo.equals(other.airportTo))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (dateFrom == null) {
			if (other.dateFrom != null)
				return false;
		} else if (!dateFrom.equals(other.dateFrom))
			return false;
		if (dateTo == null) {
			if (other.dateTo != null)
				return false;
		} else if (!dateTo.equals(other.dateTo))
			return false;
		return true;
	}
}
