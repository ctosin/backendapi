package de.cocus.flightinportugal.backendapi.domain.model.flight;

import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.cocus.flightinportugal.backendapi.utils.NumberUtils;

/**
 * Value object representing a price and its associated currency
 * 
 * @author Carlos
 */
public class Price {
	@JsonProperty("currency")
	private String currency;

	@JsonProperty("price")
	private BigDecimal value;

	public Price(String currency, BigDecimal value) {
		this.currency = currency;
		this.value = NumberUtils.toBigDecimalWithScale(value);
	}

	public String getCurrency() {
		return currency;
	}

	public BigDecimal getValue() {
		return value;
	}

	@Override
	public String toString() {
		return currency + " " + value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currency == null) ? 0 : currency.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Price other = (Price) obj;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

}
