package de.cocus.flightinportugal.backendapi.domain.service.flight;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.function.Function;

import org.springframework.stereotype.Service;

import de.cocus.flightinportugal.backendapi.domain.model.flight.Flight;
import de.cocus.flightinportugal.backendapi.domain.model.flight.FlightData;
import de.cocus.flightinportugal.backendapi.domain.model.flight.Price;
import de.cocus.flightinportugal.backendapi.utils.Constants;
import de.cocus.flightinportugal.backendapi.utils.NumberUtils;

/**
 * Average prices calculator
 * @author Carlos
 */
@Service
public class PricesCalculator {

	/**
	 * Calculates the average price for flights and bags
	 * @param flightsData Input data
	 * @param flight Flight to populate with the averages
	 * @param currency Currency to be used
	 * @param currencyRate Currency rate to convert between currency and EUR
	 */
	public void calculatePrices(List<FlightData> flightsData, Flight flight, String currency, BigDecimal currencyRate) {
		// Average price for flight
		BigDecimal avgPriceBD = getFlightsPriceAverage(flightsData);
		flight.addFlightPrice(new Price(currency, avgPriceBD));
		flight.addFlightPrice(new Price(Constants.CURRENCY_EUR, avgPriceBD.multiply(currencyRate)));
		
		// Average price for bag #1
		BigDecimal avgBag1PriceBD = getBag1AveragePrice(flightsData);
		flight.addBag1price(new Price(currency, avgBag1PriceBD));
		flight.addBag1price(new Price(Constants.CURRENCY_EUR, avgBag1PriceBD.multiply(currencyRate)));
		
		// Average price for bag #2
		BigDecimal avgBag2PriceBD = getBag2AveragePrice(flightsData);
		flight.addBag2price(new Price(currency, avgBag2PriceBD));
		flight.addBag2price(new Price(Constants.CURRENCY_EUR, avgBag2PriceBD.multiply(currencyRate)));
	}
	
	/**
	 * Calculates the Bag #1 average price
	 * @return
	 */
	private BigDecimal getBag1AveragePrice(List<FlightData> flightsData) {
		return getBagAveragePrice(flightsData, flight -> flight.getBag1Price());
	}
	
	/**
	 * Calculates the Bag #2 average price
	 * @return
	 */
	private BigDecimal getBag2AveragePrice(List<FlightData> flightsData) {
		return getBagAveragePrice(flightsData, flight -> flight.getBag2Price());
	}
	
	/**
	 * Calculates the bag average price
	 * @param func Function that returns the price to be used in calculation
	 * @return
	 */
	private BigDecimal getBagAveragePrice(List<FlightData> flightsData, Function<FlightData, BigDecimal> func) {
		int count = 0;
		BigDecimal sum = BigDecimal.ZERO;
		
		for (FlightData flightData : flightsData) {
			BigDecimal bagPrice = func.apply(flightData);
			
			// If the bag price does not exist, its value is NOT considered in the average price calculation
			if (bagPrice != null) {
				count++;
				sum = sum.add(bagPrice);
			}
		}
		
		return count == 0 ? BigDecimal.ZERO : NumberUtils.toBigDecimalWithScale(sum.divide(BigDecimal.valueOf(count), RoundingMode.HALF_EVEN));
	}
	
	/**
	 * Calculates average flight price from all the flights returned
	 * @return
	 */
	private BigDecimal getFlightsPriceAverage(List<FlightData> flightsData) {
		double avgPrice = flightsData.stream()
				.mapToDouble(flight -> flight.getPrice().doubleValue())
				.average()
				.getAsDouble();
		
		return NumberUtils.toBigDecimalWithScale(avgPrice);
	}

}
