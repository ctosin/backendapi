# BackendAPI Application - User & Developer Guide
This guide explains how the BackendAPI works and how you can do to install and run it. So let's start with the...

## Installation Instructions
You have 3 possible ways to run the BackendAPI application, which are listed below. Pick the one that suits your needs.

### 1. Using Git + Maven
The BackendAPI application is published in a public **GitLab** repository ([this is the repository link](https://gitlab.com/ctosin/backendapi)). You can clone the repository and use Maven to build and run it.

To accomplish this step, you must have the **Git command-line** tools installed. In case you do not have them, [click here](https://git-scm.com/download) to download and install.

Also, you must have **JDK 13.0.1** (at least) installed in your computer. Also, double check if you have a *JAVA_HOME* environment variable pointing to the JDK folder and if the *bin* folder from the JDK is added to the *PATH* variable.

OK, so these are the steps...

Open a command-line interface (Linux shell, Windows command prompt or whatever...) and navigate to a folder that will host the cloned repository:
```
cd [repository_folder]
```
Then clone the repository:
```
git clone https://gitlab.com/ctosin/backendapi.git .
```
Now enter the directory and build the project using Maven:
```
cd backendapi
mvnw install
```
After the build is complete you should have a *target* folder inside the repository folder. Now you can run the application:
```
java -jar java -jar ./target/backendapi-0.0.1-SNAPSHOT.jar
```
You are ready! :) Now you can invoke the REST endpoint from your machine:
```
http://localhost:8080/flight/avg?[params...]
```

### 2. Using a Docker Image
Another way of running the BackendAPI application is through Docker. In this scenario, you first need to install Docker ([click here](https://www.docker.com) to go to Docker website).

When Docker is installed and running, download the *docker-compose.yml* file from [here](https://gitlab.com/ctosin/backendapi/-/blob/master/backendapi/docker-compose.yml) and save it in a known location.

In your Docker Terminal, `cd` to the directory where you saved the file and execute:
```
docker-compose up
```
That command itself analyzes the *docker-compose.yml* file, downloads all the necessary images and runs the application.

### 3. Git + Eclipse
The Maven BackendAPI project can be easily imported in Eclipse. You can use the *Git perspective* from Eclipse to clone the repository (you can find more information on how to do that [here](https://www.vogella.com/tutorials/EclipseGit/article.html)).

Another option you have is to download the project as a ZIP file from [here](https://gitlab.com/ctosin/backendapi/-/archive/master/backendapi-master.zip) and use `File > Import > Maven > Existing Maven Projects` option from the Eclipse menu to import the project in an Eclipse Workspace.

### Note on the Database Setup
#### PostgreSQL
If you choose the Docker Image approach, you do not have to worry about the database, since Docker automatically sets up a PostgreSQL container for the application to use.

However, if you want to run the application outside Docker you have to manually provide a PostgreSQL database for the application. The default application configuration tries to connect to PostgreSQL using these parameters:

 - `server`: localhost
 - `port`: 5432
 - `database`: postgres
 - `username`: postgres
 - `password`: admin

If you need to use different PostgreSQL parameters, you can override application defaults through command-line arguments when starting the application. You can have more information on how to do that [here](https://www.baeldung.com/spring-boot-command-line-arguments).

#### H2
If you do not want to bother about setting up a PostgreSQL database, an easier solution is to rely on H2, an in-memory database shipped with the application.

To make it work, you just have to start the BackendAPI application using the *test* profile, which uses H2 instead of PostgreSQL. Just use the command below to start the application and you are all set:

```
java -Dspring.profiles.active=test -jar ./target/backendapi-0.0.1-SNAPSHOT.jar
```

### Live application hosted in AWS
The BackendAPI application is currently running in an **AWS** environment for testing purposes, with a **Docker** container in **Elastic Beanstalk** that calls a PostgreSQL database in **Amazon RDS**.

To access the application from the internet, use the following URL:
```
http://backendapi.sa-east-1.elasticbeanstalk.com/flight/avg?[params...]
```


## Application Endpoints
The BackendAPI application is essentially a RESTful web service, which exposes three types of operations.

### Get the flight average prices
This operation calculates the average of flight and bags prices from all flights of a route during a specified time frame.

#### Request
**GET** /flight/avg

|Parameter|Type|Format|Size|Required|Description
|--|--|--|--|--|--|
|`airportFrom`|string|-|3|true|The 3-letter code (IATA) departure airport|
|`airportTo`|string|-|3|true|The 3-letter code (IATA) arrival airport|
|`dateFrom`|date|dd/MM/yyyy|10|true|Departure date|
|`dateTo`|date|dd/MM/yyyy|10|true|Arrival date|
|`currency`|string|-|3|true|The 3-letter currency code to convert average prices|

#### Response
Property|Description|Type|Size|Format|
|--|--|--|--|--|
`code`|HTTP return code|integer|3|-|
`success`|Whether the response was successful or not|boolean|-|-|
`data`|List of `Flight` (see below). Only present if `success` is `true`|array of `Flight`|*|-|
`errors`|List of errors (only present is `success` is `false`)|array of string|*|-|
##### Flight
Property|Description|Type|Size|Format|
|--|--|--|--|--|
`date_from`|Provided departure date|date|10|dd/MM/yyyy|
`date_to`|Provided arrival date|date|10|dd/MM/yyyy|
`airport_from`|Provided departure airport|string|3|-|
`airport_to`|Provided arrival airport|string|3|-|
`flight_price_avg`|List of `Price`, representing the average flight prices in EUR and `currency` currencies|*|-|-|
`bag1_price_avg`|List of `Price`, representing the average bag #1 prices in EUR and `currency` currencies|*|-|-|
`bag2_price_avg`|List of `Price`, representing the average bag #2 prices in EUR and `currency` currencies|*|-|-|

##### Price
Property|Description|Type|Size|Format|
|--|--|--|--|--|
`currency`|Currency associated to the price|string|3|-|
`price`|Price|double|-|-|

#### Response Example
```
{
  "code": 200,
  "success": true,
  "data": {
    "date_from": "01/04/2020",
    "date_to": "02/04/2020",
    "airport_from": "LIS",
    "airport_to": "OPO",
    "flight_price_avg":
    [
      {
        "currency": "BRL",
        "price": 599.43
      },
      {
        "currency": "EUR",
        "price": 114.78
      }
    ],
    "bag1_price_avg":
    [
      {
        "currency": "BRL",
        "price": 54.40
      },
      {
        "currency": "EUR",
        "price": 10.42
      }
    ],
    "bag2_price_avg":
    [
      {
        "currency": "BRL",
        "price": 114.39
      },
      {
        "currency": "EUR",
        "price": 21.90
      }
    ]
  }
}
```

### List all the request logs
This operation lists all the request logs stored in the application database.


#### Request
**GET** /flight/logs

|Parameter|Type|Size|Required|
|--|--|--|--|
|None|-|-|-|

#### Response
Property|Description|Type|Size|Format|
|--|--|--|--|--|
`code`|HTTP return code|integer|3|-|
`success`|Whether the response was successful or not|boolean|-|-|
`data`|List of `RequestLog` (see below). Only present if `success` is `true`|array of `RequestLog`|*|-|
`errors`|List of errors (only present is `success` is `false`)|array of string|*|-|


##### Request Log
Property|Description|Type|Size|Format|
|--|--|--|--|--|
`id`|Request log ID|integer|-|-|
`logTime`|Log timestamp|datetime|-|{yyyy-MM-dd}T{HH:mm:ss.MMMMMM}|
`ipAddress`|Request IP address|string|15|999.999.999.999|
`requestData`|Data sent during the request|string|-|JSON format|
`responseData`|Data sent in response|string|-|JSON format|
`cachedResponse`|Whether the response was retrieved from the cache|boolean|-|-|

#### Response Example
```
{
  "code": 200,
  "success": true,
  "data": [
  {
    "id": 1,
    "logTime": "2020-03-08T12:23:17.781793",
    "ipAddress": "192.168.99.100",
    "requestData": "{\"airportFrom\":\"LIS\",\"airportTo\":\"OPO\",\"dateFrom\":{\"year\":2020,\"month\":\"APRIL\",\"monthValue\":4,\"dayOfMonth\":1,\"chronology\":{\"calendarType\":\"iso8601\",\"id\":\"ISO\"},\"dayOfWeek\":\"WEDNESDAY\",\"leapYear\":true,\"dayOfYear\":92,\"era\":\"CE\"},\"dateTo\":{\"year\":2020,\"month\":\"APRIL\",\"monthValue\":4,\"dayOfMonth\":2,\"chronology\":{\"calendarType\":\"iso8601\",\"id\":\"ISO\"},\"dayOfWeek\":\"THURSDAY\",\"leapYear\":true,\"dayOfYear\":93,\"era\":\"CE\"},\"currency\":\"BRL\",\"ipAddress\":\"192.168.99.1\"}",
    "responseData": "{\"date_from\":{\"year\":2020,\"month\":\"APRIL\",\"monthValue\":4,\"dayOfMonth\":1,\"chronology\":{\"calendarType\":\"iso8601\",\"id\":\"ISO\"},\"dayOfWeek\":\"WEDNESDAY\",\"leapYear\":true,\"dayOfYear\":92,\"era\":\"CE\"},\"date_to\":{\"year\":2020,\"month\":\"APRIL\",\"monthValue\":4,\"dayOfMonth\":2,\"chronology\":{\"calendarType\":\"iso8601\",\"id\":\"ISO\"},\"dayOfWeek\":\"THURSDAY\",\"leapYear\":true,\"dayOfYear\":93,\"era\":\"CE\"},\"airport_from\":\"LIS\",\"airport_to\":\"OPO\",\"flight_price_avg\":[{\"currency\":\"BRL\",\"price\":605.98},{\"currency\":\"EUR\",\"price\":116.03}],\"bag1_price_avg\":[{\"currency\":\"BRL\",\"price\":59.50},{\"currency\":\"EUR\",\"price\":11.39}],\"bag2_price_avg\":[{\"currency\":\"BRL\",\"price\":111.40},{\"currency\":\"EUR\",\"price\":21.33}]}",
    "cachedResponse": false
  }]
}
```

### Delete all the request logs
This operation deletes all the request logs stored in the application database.

#### Request
**DELETE** /flight/logs

|Parameter|Type|Size|Required|
|--|--|--|--|
|None|-|-|-|

#### Response
Property|Description|Type|Size|Format|
|--|--|--|--|--|
`code`|HTTP return code|integer|3|-|
`success`|Wheter the response was successful or not|boolean|-|-|
`errors`|List of errors (only present is `success` is `false`)|array of string|*|-|

#### Response Example
```
{
  "code": 200,
  "success": true
}
```
## Business Decisions
This section is related to some business decisions taken while creating the BackendAPI application.

### Airports and Flight Companies
According to the solution specification document, the BackendAPI application was supposed to provide flight information considering *Lisbon* and *Oporto* airports from *TAP* and *Ryanair* companies:

> The company Flight-In-Portugal needs a backend API to provide to external usage. What we are expecting is a new API that provides flights information between Oporto -> Lisbon or Lisbon -> Oporto from the company TAP and Ryanair.

 I chose to not restrict airports and flights to the ones stated above. That way, the BackendAPI application is ready to support a wider range of flights. It works perfectly for *Lisbon -> Oporto* and *Oporto -> Lisbon*, but it works for other airports as well.

Moreover, if another flight company starts to operate those routes, the application will take them into account immediately.

### Average Prices
The most important task of the BackendAPI application is to calculate the average prices from flights.

These are the parameters used to search for flights:

 - `airportFrom`: the departure airport
 - `airportTo`: the arrival airport
 - `dateFrom`: the departure date
 - `dateTo`: the arrival date
 - `currency`: the currency to convert prices

For example, suppose that these parameters are provided:

 - `airportFrom`: LIS
 - `airportTo`: OPO
 - `dateFrom`: 01/04/2020
 - `dateTo`: 02/04/2020
 - `currency`: BRL

The BackendAPI application will query [Kiwi](https://docs.kiwi.com/#flights) for all *LIS -> OPO* flights from *01/04/2020* to *02/04/2020* from all flight companies. The result is a list of flights information.

Let's suppose the search resulted in 3 flights with the following price data:

- **<u>Flight #1</u>**
  - Flight price: **€ 24**
  - Bag #1 price: **€ 13**
  - Bag #2 price: **€ 9**
- **<u>Flight #2</u>**
  - Flight price: **€ 30**
  - Bag #1 price: **€ 12**
  - Bag #2 price: **€ 10**
- **<u>Flight #3</u>**
  - Flight price: **€ 25**
  - Bag #1 price: **€ 15**
  - Bag #2 price: **€ 12**

To calculate the average prices, the application sums the prices and divide by the number of flights. The result will be a flight price, a bag #1 price and a bag #2 price.

In the case above, the resulting average calculation would be:

- Flight price:  **€ 26.33**
- Bag #1 price:  **€ 13.33**
- Bag #2 price:  **€ 10.33**


#### Bag #2 Not Allowed
For some flights, carrying a 2<sup>nd</sup> bag is not allowed. The Kiwi web service only returns the price for the 1<sup>st</sup> bag.

That issue needed to be addressed because it impacts how the average prices are calculated. So in a scenario like that, the bag #2 is not considered in the average.

For example, suppose that the flights below were returned from Kiwi:

- **<u>Flight #1</u>**
  - Flight price: **€ 24**
  - Bag #1 price: **€ 13**
  - Bag #2 price: **€ 9**
- **<u>Flight #2</u>**
  - Flight price: **€ 30**
  - Bag #1 price: **€ 12**
  - Bag #2 price: **NOT PROVIDED**
- **<u>Flight #3</u>**
  - Flight price: **€ 25**
  - Bag #1 price: **€ 15**
  - Bag #2 price: **€ 12**
  
When calculating the bag #2 average price, the application will not consider Flight #2 (it will use only Flights #1 and #3 to calculate the average). The result would be:

- Flight price:  **€ 26.33**
- Bag #1 price:  **€ 13.33**
- Bag #2 price:  **€ 10.50**
 
 #### Currency Conversion
When the BackendAPI application is asked to calculate the average prices, a currency is provided. The application returns prices in that currency as well as in EUR currency.

Let's suppose that the average flight price was € 80 and the client informed that the prices needed to be returned in BRL currency. The application will return both prices to the client:

- 80 (EUR)
- 421.60 (BRL)
 
 The currency conversion rate to do the calculation is provided by Kiwi.
 

## Implementation Decisions
This section provides information about some of the implementation aspects of the application.

### Technology
These are the chosen technology for BackendAPI application:

- Java 13 (last stable Java version)
- Spring Boot (+Spring Web to provide support to RESTful web services)
- Maven (dependency management)
- PosgreSQL database (to store request log information)
- H2 database (can optionally replace PostgreSQL to store log data)

### Application Components & External Components
The component diagram below shows the components involved in the solution:

![enter image description here](http://static.softblue.com.br/cocus/uml_components_3.png)

There is only one REST controller (`FlightController`), which exposes all the application services through the internet.

The `LogService` provides an interface to manage request logs, which are stored in an external database.

The `FlightService` communicates with the external Kiwi web service through the `KiwiServiceProxy` to search for flights. Then it delegates the average prices calculation to the `PricesCalculator` business service.

### Kiwi Web Service Cache
BackendAPI supports response cache from Kiwi Web Service requests. The cache time is configured in *application.properties* file:

```
# Data cache time (in seconds) for Kiwi web service (0 means no cache)
app.kiwi.cachedProxy.expirationTimeInSecs=10
```
The relevant classes of the cache architecture can be seen in the following class diagram:

![enter image description here](http://static.softblue.com.br/cocus/uml_cache2.png)

The `KiwiServiceCachedProxy` class extends `KiwiServiceProxy` and provides the cache functionality. To decide which class will be used by `FlightService`, Spring Boot dependency injection system calls `BackendapiApplication.createKiwiServiceProxy()`, which reads the cache property from *application.properties* and returns the correct object.

### Domain-Driven Design
The code follows a DDD (Domain-Driven Design) approach. The package diagram below shows how Java packages were organized:

![enter image description here](http://static.softblue.com.br/cocus/uml_packages.png)

According to DDD, application parts can be divided into application, domain and infrastructure layers.

#### Application Layer
This layer has the application services (i.e. `FlightService` and `LogService`) and acts as a bridge between the model and the infrastructure layer.

#### Infrastructure Layer
This layer hosts code related to specific technologies. Here you will find the `FlightSearchController`, which is a Spring REST controller; and `KiwiServiceProxy`, which proxies the `FlightService` call to the external Kiwi flight search web service.

#### Domain Layer
This layer represents the domain logic and it is infrastructure independent. Here you will find the `RequestLog` entity, along with its repository `RequestLogRepository`; and a bunch of classes that represent *value objects*, such as `Flight`, `FlightData` and `Price`.

In the domain layer you can also find the `PricesCalculator`, which is a business service responsible for calculating average prices from returned flights.

*Actually, `RequestLog` and `RequestLogRepository` classes are currently referring some JPA specific artifacts, what is supposed to break the DDD. Since the application logic is simple, I chose to do that because 1) I did not think it would bring considerable problems; and 2) I did not think it would be necessary to add more complexity to the code. The same is valid for service classes, which use the Spring `@Service` annotation.*

### Spring Boot Profiles
The BackendAPI application has 3 profiles:

- **default (no profile)**: Used for development mode. It connects to a localhost PostgreSQL database.
- **test**: Used for unit and integration tests. It connects to an H2 in-memory database.
- **prod**: Used for production, i.e. to run in a Docker container. It connects to a known PostgreSQL database host, which is in another container.


### Unit and Integration Testing
There are several test classes written to test the application. The tests focus on testing the web service controller behavior (`FlightSearchController`), the `LogService` and `FlightService` application services and the `PricesCalculator` business service.
